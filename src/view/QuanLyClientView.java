
package view;

import controller.QuanLyClientController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import model.QuanLyClientModel;
import model.ThuMucModel;

public class QuanLyClientView extends JFrame implements ICommon {
    private String[] TenCot = { "<html><center>STT", "<html><center>Thời điểm", "<html><center>Hành động", "<html><center>Diễn giải" };
    private String[][] KetQuaTimKiem = new String[][]{};
    
    private QuanLyClientModel QLC_MD;
    
    private SpringLayout Layout;
    
    private JPanel jPanel_Top;
    private JPanel jPanel_IP_Port;
    private JPanel jPanel_TrangThai;
    private JPanel jPanel_Left;
    private JPanel jPanel_Center;
    private JPanel jPanel_Center_Top;
    private JPanel jPanel_Center_NoiDung;
    private JPanel jPanel_CayThuMuc;
    
    private JLabel jLabel_TrangThai;
    
    private JButton jButton_KetNoi;
    private JButton jButton_NgatKetNoi;
    private JButton jButton_TimKiem_LichSu;
    private JButton jButton_DongMo_LichSu;
    
    private JTextField jTextField_Ip;
    private JTextField jTextField_Port;
    private JTextField jTextField_TimKiem_LichSu;
    private JTextField jTextField_DiaChi_CayThuMuc;
    
    private JTable jTable_LichSu;
    
    public QuanLyClientView() {
        initComponent();
        addComponent();
        QLC_MD = new QuanLyClientModel();
        addEvent();
        KetQuaTimKiem = QLC_MD.LayDanhSach_TapTinLog();
    }

    @Override
    public void initComponent() {
        setDefaultLookAndFeelDecorated(true);
        setTitle("Quản Lý Máy Người Dùng");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1100, 600);
        setPreferredSize(new Dimension(1100, 600));
        setLocationRelativeTo(null);
        setBackground(Color.WHITE);
        setResizable(true);
    }

    @Override
    public void addComponent() {
        Layout = new SpringLayout();
        JPanel jPanel = new JPanel(new BorderLayout());
        
        // JPanel Top
        jPanel_Top = JPanel_Top();
        jPanel_IP_Port = JPanel_IP_Port();
        jPanel_TrangThai = JPanel_TrangThai();
        jPanel_Top.add(jPanel_IP_Port);
        jPanel_Top.add(jPanel_TrangThai);
        
        // JPanel Left
        jPanel_Left = JPanel_Left();
        JScrollPane Scroll_Left = new JScrollPane(jPanel_Left,
                            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        // JPanel Center
        jPanel_Center = JPanel_Center();
        
        jPanel.add(jPanel_Top, BorderLayout.NORTH);
        jPanel.add(Scroll_Left, BorderLayout.WEST);
        jPanel.add(jPanel_Center, BorderLayout.CENTER);
        
        add(jPanel);
    }

    @Override
    public void addEvent() {
        QuanLyClientController qlc = new QuanLyClientController(this, QLC_MD);
        jButton_DongMo_LichSu.addActionListener(qlc);
        jButton_KetNoi.addActionListener(qlc);
        jButton_NgatKetNoi.addActionListener(qlc);
        addWindowListener(qlc);
    }
    
    private JPanel JPanel_Top() {
        JPanel jPanel = new JPanel(new GridLayout(1, 2, 1, 1));
        jPanel.setSize(1100, 100);
        jPanel.setPreferredSize(new Dimension(1100, 100));
        return jPanel;
    }
    
    private JPanel JPanel_Left() {
        int width = 400;
        Border border = BorderFactory.createTitledBorder(null, "Lịch sử thao tác", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION);
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBackground(Color.red);
        jPanel.setOpaque(false);
        jPanel.setSize(width, this.getHeight() - 140);
        jPanel.setPreferredSize(new Dimension(width, this.getHeight() - 140));
        jPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        JPanel jPanel_TimKiem = new JPanel(Layout);
        jPanel_TimKiem.setSize(new Dimension(width, 40));
        jPanel_TimKiem.setPreferredSize(new Dimension(width, 40));
        jPanel_TimKiem.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        jTextField_TimKiem_LichSu = new JTextField();
        jTextField_TimKiem_LichSu.setSize(new Dimension(300, 40));
        jTextField_TimKiem_LichSu.setPreferredSize(new Dimension(300, 40));
        
        jButton_TimKiem_LichSu = new JButton("Tìm");
        jButton_TimKiem_LichSu.setSize(new Dimension(60, 40));
        jButton_TimKiem_LichSu.setPreferredSize(new Dimension(60, 40));
        
        // Xét Layout cho Component
        Layout.putConstraint(SpringLayout.NORTH, jTextField_TimKiem_LichSu, 2, SpringLayout.NORTH, jPanel_TimKiem);
        Layout.putConstraint(SpringLayout.SOUTH, jTextField_TimKiem_LichSu, -2, SpringLayout.SOUTH, jPanel_TimKiem);
        Layout.putConstraint(SpringLayout.WEST, jTextField_TimKiem_LichSu, 2, SpringLayout.WEST, jPanel_TimKiem);
        Layout.putConstraint(SpringLayout.EAST, jTextField_TimKiem_LichSu, 0, SpringLayout.WEST, jButton_TimKiem_LichSu);
        Layout.putConstraint(SpringLayout.NORTH, jButton_TimKiem_LichSu, 2, SpringLayout.NORTH, jPanel_TimKiem);
        Layout.putConstraint(SpringLayout.SOUTH, jButton_TimKiem_LichSu, -2, SpringLayout.SOUTH, jPanel_TimKiem);
        Layout.putConstraint(SpringLayout.EAST, jButton_TimKiem_LichSu, -2, SpringLayout.EAST, jPanel_TimKiem);
        
        jPanel_TimKiem.add(jTextField_TimKiem_LichSu);
        jPanel_TimKiem.add(jButton_TimKiem_LichSu);
        
        JPanel jPanel_LichSu = new JPanel(Layout);
        jPanel_LichSu.setBorder(border);
        
        DefaultTableModel model = new DefaultTableModel(KetQuaTimKiem, TenCot);
        jTable_LichSu = new JTable(model) {
            private static final long serialVersionUID = 1L;
            public boolean isCellEditable(int row, int column) { return false; };
        };
        jTable_LichSu.setBounds(30, 40, 200, 300);
        jTable_LichSu.getTableHeader().setBackground(new Color(110, 195, 201));
        jTable_LichSu.getTableHeader().setPreferredSize(new Dimension(50, 40));
        jTable_LichSu.setBackground(new Color(202, 229, 232));
        jTable_LichSu.setRowHeight(50);
        jTable_LichSu.setFillsViewportHeight(true);
        
        JScrollPane jScrollPane_LichSu = new JScrollPane(jTable_LichSu);
        jScrollPane_LichSu.setViewportView(jTable_LichSu);
        
        Layout.putConstraint(SpringLayout.NORTH, jScrollPane_LichSu, 2, SpringLayout.NORTH, jPanel_LichSu);
        Layout.putConstraint(SpringLayout.SOUTH, jScrollPane_LichSu, -2, SpringLayout.SOUTH, jPanel_LichSu);
        Layout.putConstraint(SpringLayout.WEST, jScrollPane_LichSu, 2, SpringLayout.WEST, jPanel_LichSu);
        Layout.putConstraint(SpringLayout.EAST, jScrollPane_LichSu, -2, SpringLayout.EAST, jPanel_LichSu);
        
        jPanel_LichSu.add(jScrollPane_LichSu);
        
        jPanel.add(jPanel_TimKiem, BorderLayout.NORTH);
        jPanel.add(jPanel_LichSu, BorderLayout.CENTER);
        
        return jPanel;
    }
    
    private JPanel JPanel_Center() {
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setSize(800, this.getHeight() - 100);
        jPanel.setPreferredSize(new Dimension(800, this.getHeight() - 100));
        
        jPanel_Center_Top = JPanel_Center_Top();
        jPanel_Center_NoiDung = JPanel_Center_NoiDung();
        
        jPanel.add(jPanel_Center_Top, BorderLayout.NORTH);
        jPanel.add(jPanel_Center_NoiDung, BorderLayout.CENTER);
        
        return jPanel;
    }
    
    private JPanel JPanel_Center_Top() {
        int height = 30;
        JPanel jPanel = new JPanel(Layout);
        jPanel.setSize(800, height);
        jPanel.setPreferredSize(new Dimension(800, height));
        jPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        jButton_DongMo_LichSu = new JButton("-");
        jButton_DongMo_LichSu.setSize(height, height);
        jButton_DongMo_LichSu.setPreferredSize(new Dimension(height, height));
        
        jTextField_DiaChi_CayThuMuc = new JTextField();
        jTextField_DiaChi_CayThuMuc.setSize(770, height-10);
        jTextField_DiaChi_CayThuMuc.setPreferredSize(new Dimension(770, height-10));
        
        // Xét Layout cho Component
        Layout.putConstraint(SpringLayout.NORTH, jButton_DongMo_LichSu, 1, SpringLayout.NORTH, jPanel);
        Layout.putConstraint(SpringLayout.WEST, jButton_DongMo_LichSu, 1, SpringLayout.WEST, jPanel);
        Layout.putConstraint(SpringLayout.SOUTH, jButton_DongMo_LichSu, -1, SpringLayout.SOUTH, jPanel);
        
        Layout.putConstraint(SpringLayout.WEST, jTextField_DiaChi_CayThuMuc, 2, SpringLayout.EAST, jButton_DongMo_LichSu);
        Layout.putConstraint(SpringLayout.NORTH, jTextField_DiaChi_CayThuMuc, 1, SpringLayout.NORTH, jPanel);
        Layout.putConstraint(SpringLayout.EAST, jTextField_DiaChi_CayThuMuc, -1, SpringLayout.EAST, jPanel);
        Layout.putConstraint(SpringLayout.SOUTH, jTextField_DiaChi_CayThuMuc, 0, SpringLayout.SOUTH, jPanel);
        
        jPanel.add(jButton_DongMo_LichSu);
        jPanel.add(jTextField_DiaChi_CayThuMuc);
        
        return jPanel;
    }
    
    private JPanel JPanel_Center_NoiDung() {
        int height = this.getHeight() - 130;
        JPanel jPanel = new JPanel();
        jPanel.setSize(800, height);
        jPanel.setPreferredSize(new Dimension(800, height));
        jPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jPanel.setBackground(Color.WHITE);
        
        return jPanel;
    }
    
    private JPanel JPanel_IP_Port() {
        Layout = new SpringLayout();
        int width_lb = 40;
        int width_tf = 200;
        int height = 25;

        JPanel jPanel = new JPanel(Layout);
        Border border = BorderFactory.createTitledBorder(null, "Thông Tin Máy Người Dùng", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION);
        jPanel.setBorder(border);
        
        // Thiết lập JLabel và JTextField Ip
        JLabel jLabel_Ip = new JLabel("IP");
        jLabel_Ip.setSize(width_lb, height);
        jLabel_Ip.setPreferredSize(new Dimension(width_lb, height));
        jTextField_Ip = new JTextField("172.31.80.1");
        jTextField_Ip.setCaretPosition(jTextField_Ip.getText().length());
        jTextField_Ip.setSize(width_tf, height);
        jTextField_Ip.setPreferredSize(new Dimension(width_tf, height));
        
        // Thiết lập JLabel và JTextField Port
        JLabel jLabel_Port = new JLabel("PORT");
        jLabel_Port.setSize(width_lb, height);
        jLabel_Port.setPreferredSize(new Dimension(width_lb, height));
        jTextField_Port = new JTextField("6543");
        jTextField_Port.setCaretPosition(jTextField_Port.getText().length());
        jTextField_Port.setSize(width_tf, height);
        jTextField_Port.setPreferredSize(new Dimension(width_tf, height));

        // Xét Layout cho Component
        Layout.putConstraint(SpringLayout.NORTH, jLabel_Ip, 10, SpringLayout.NORTH, jPanel);
        Layout.putConstraint(SpringLayout.WEST, jLabel_Ip, 10, SpringLayout.WEST, jPanel);
        Layout.putConstraint(SpringLayout.WEST, jTextField_Ip, 5, SpringLayout.EAST, jLabel_Ip);
        Layout.putConstraint(SpringLayout.NORTH, jTextField_Ip, 10, SpringLayout.NORTH, jPanel);
        Layout.putConstraint(SpringLayout.EAST, jTextField_Ip, -10, SpringLayout.EAST, jPanel);

        Layout.putConstraint(SpringLayout.NORTH, jLabel_Port, 7, SpringLayout.SOUTH, jLabel_Ip);
        Layout.putConstraint(SpringLayout.WEST, jLabel_Port, 10, SpringLayout.WEST, jPanel);
        Layout.putConstraint(SpringLayout.WEST, jTextField_Port, 5, SpringLayout.EAST, jLabel_Port);
        Layout.putConstraint(SpringLayout.NORTH, jTextField_Port, 7, SpringLayout.SOUTH, jTextField_Ip);
        Layout.putConstraint(SpringLayout.EAST, jTextField_Port, -10, SpringLayout.EAST, jPanel);
        Layout.putConstraint(SpringLayout.SOUTH, jTextField_Port, -10, SpringLayout.SOUTH, jPanel);

        jPanel.add(jLabel_Ip);
        jPanel.add(jTextField_Ip);
        jPanel.add(jLabel_Port);
        jPanel.add(jTextField_Port);
        
        return jPanel;
    }
    
    private JPanel JPanel_TrangThai() {
        Layout = new SpringLayout();
        int width = 150;
        int height = 25;
        JPanel jPanel = new JPanel(Layout);
        Border border = BorderFactory.createTitledBorder(null, "Trạng thái", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION);
        jPanel.setBorder(border);
        
        JPanel jPanel_NutBam = new JPanel(new GridLayout(1, 2, 1, 1));
        jButton_KetNoi = new JButton("Start");
        jButton_KetNoi.setPreferredSize(new Dimension(width, height));
        jButton_NgatKetNoi = new JButton("Stop");
        jButton_NgatKetNoi.setEnabled(false);
        jButton_NgatKetNoi.setPreferredSize(new Dimension(width, height));
        jLabel_TrangThai = new JLabel("Disconnected.");
        jLabel_TrangThai.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jLabel_TrangThai.setHorizontalAlignment(JTextField.CENTER);
        jPanel_NutBam.add(jButton_KetNoi);
        jPanel_NutBam.add(jButton_NgatKetNoi);
        
        Layout.putConstraint(SpringLayout.NORTH, jPanel_NutBam, 10, SpringLayout.NORTH, jPanel);
        Layout.putConstraint(SpringLayout.WEST, jPanel_NutBam, 5, SpringLayout.WEST, jPanel);
        Layout.putConstraint(SpringLayout.NORTH, jPanel_NutBam, 10, SpringLayout.NORTH, jPanel);
        Layout.putConstraint(SpringLayout.EAST, jPanel_NutBam, -5, SpringLayout.EAST, jPanel);
        
        Layout.putConstraint(SpringLayout.NORTH, jLabel_TrangThai, 5, SpringLayout.SOUTH, jPanel_NutBam);
        Layout.putConstraint(SpringLayout.WEST, jLabel_TrangThai, 5, SpringLayout.WEST, jPanel);
        Layout.putConstraint(SpringLayout.EAST, jLabel_TrangThai, -5, SpringLayout.EAST, jPanel);
        Layout.putConstraint(SpringLayout.SOUTH, jLabel_TrangThai, -5, SpringLayout.SOUTH, jPanel);
        
        jPanel.add(jPanel_NutBam);
        jPanel.add(jLabel_TrangThai);
        return jPanel;
    }
    
    public void Set_DiaChi_CayThuMuc(String ten) {
        jTextField_DiaChi_CayThuMuc.setText(ten);
    }
    
    public void HienThiCayThuMuc(CayThuMucView caythumuc) {
        jPanel_Center_NoiDung.removeAll();
        jPanel_Center_NoiDung.add(caythumuc);
        jPanel_Center_NoiDung.revalidate();
        jPanel_Center_NoiDung.repaint();
    }
    
    public void AnHien_TrangThai(boolean flag) {
        if(flag){
            jButton_KetNoi.setEnabled(false);
            jButton_NgatKetNoi.setEnabled(true);
            jTextField_Ip.setEnabled(false);
            jTextField_Port.setEnabled(false);
        } else {
            jButton_KetNoi.setEnabled(true);
            jButton_NgatKetNoi.setEnabled(false);
            jTextField_Ip.setEnabled(true);
            jTextField_Port.setEnabled(true);
        }
    }
    
    public void CapNhapLog(String[][] dt){
        jTable_LichSu.setModel(new DefaultTableModel(dt, TenCot));
    }
    
    public void CapNhat_ThongBao_TrangThai(boolean flag){
        if(flag)
            jLabel_TrangThai.setText("Connected to Server...");
        else
            jLabel_TrangThai.setText("Disconnected.");
    }
    
    public String LayPort() {
        return jTextField_Port.getText();
    }
    
    public String LayIP() {
        return jTextField_Ip.getText();
    }
    
    public void AnHien_DanhSach_LichSu(boolean flag) {
        if(flag) {
            jButton_DongMo_LichSu.setText("+");
            jPanel_Left.setSize(new Dimension(0, 0));
            jPanel_Left.setPreferredSize(new Dimension(0, 0));
            jPanel_Left.revalidate();
            jPanel_Left.repaint();
        } else {
            jButton_DongMo_LichSu.setText("-");
            jPanel_Left.setSize(new Dimension(300, this.getHeight() - 140));
            jPanel_Left.setPreferredSize(new Dimension(300, this.getHeight() - 140));
            jPanel_Left.revalidate();
            jPanel_Left.repaint();
        }
    }
    
    public void Dialog_ThongBao(String noidung) {
        JOptionPane.showMessageDialog(null,
            noidung,
            "Thông báo",
            JOptionPane.INFORMATION_MESSAGE
        );
    }
}
