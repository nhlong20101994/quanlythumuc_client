/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import model.TapTinModel;
import model.ThuMucModel;

/**
 *
 * @author ASUS
 */
public class CayThuMucView extends JPanel implements ICommon {

    private QuanLyClientView QLC_V;
    private SpringLayout Layout;
    private ThuMucModel CayThuMuc;

    private JTree tree;

    public CayThuMucView(ThuMucModel caythumuc, QuanLyClientView qlc_v) {
        CayThuMuc = caythumuc;
        QLC_V = qlc_v;
        initComponent();
        addComponent();
        addEvent();
    }

    @Override
    public void initComponent() {
        setSize(700, 600);
        setPreferredSize(new Dimension(700, 600));
    }

    @Override
    public void addComponent() {
        Layout = new SpringLayout();
        this.setLayout(Layout);
        //create the root node
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(CayThuMuc.LayTenThuMuc());
        TaiThuMuc_TapTin(root, CayThuMuc);

        tree = new JTree(root);
        JScrollPane Scroll_Tree = new JScrollPane(tree,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        tree.setShowsRootHandles(true);

        // Xét Layout cho Component
        Layout.putConstraint(SpringLayout.NORTH, Scroll_Tree, 0, SpringLayout.NORTH, this);
        Layout.putConstraint(SpringLayout.SOUTH, Scroll_Tree, 2, SpringLayout.SOUTH, this);
        Layout.putConstraint(SpringLayout.WEST, Scroll_Tree, 0, SpringLayout.WEST, this);
        Layout.putConstraint(SpringLayout.EAST, Scroll_Tree, 0, SpringLayout.EAST, this);

        add(Scroll_Tree);
    }

    @Override
    public void addEvent() {
        tree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                String DiaChi = "," + e.getPath().toString();
                DiaChi = DiaChi.replace("[", "");
                DiaChi = DiaChi.replace("]", "");
                DiaChi = DiaChi.replace(",", " > ");
                QLC_V.Set_DiaChi_CayThuMuc(DiaChi);
            }
        });
    }

    private void TaiThuMuc_TapTin(DefaultMutableTreeNode root, ThuMucModel caythumuc) {
        for (ThuMucModel tm : caythumuc.LayDanhSach_ThuMuc()) {
            DefaultMutableTreeNode Node_ThuMuc = new DefaultMutableTreeNode(tm.LayTenThuMuc());
            root.add(Node_ThuMuc);
            TaiThuMuc_TapTin(Node_ThuMuc, tm);
        }
        for (TapTinModel tt : caythumuc.LayDanhSach_TapTin()) {
            DefaultMutableTreeNode Node_TapTin = new DefaultMutableTreeNode(tt.LayTenTapTin() + "." + tt.LayDinhDang());
            root.add(Node_TapTin);
        }
    }
}
