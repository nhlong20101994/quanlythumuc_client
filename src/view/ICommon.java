package view;

public interface ICommon {
    void initComponent();
    void addComponent();
    void addEvent();
}