package controller;

import java.awt.event.*;
import model.QuanLyClientModel;
import model.ThreadLangNgheThayDoiTapTin;
import model.ThuMucModel;
import view.CayThuMucView;
import view.QuanLyClientView;

public class QuanLyClientController implements ActionListener, WindowListener {

    private QuanLyClientView QLC_V;
    private QuanLyClientModel QLC_MD;

    public QuanLyClientController(QuanLyClientView qlc_v, QuanLyClientModel qlc_md) {
        this.QLC_V = qlc_v;
        this.QLC_MD = qlc_md;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String src = e.getActionCommand();
        switch (src) {
            case "-":
                QLC_V.AnHien_DanhSach_LichSu(true);
                return;
            case "+":
                QLC_V.AnHien_DanhSach_LichSu(false);
                return;
            case "Start":
                String port_S = QLC_V.LayPort();
                String ip_S = QLC_V.LayIP();
                if (port_S.equals("") || ip_S.equals("")) {
                    QLC_V.Dialog_ThongBao("IP hoặc PORT không được để trống!!!");
                } else {
                    int port_I = (KiemTraSo(port_S) ? Integer.parseInt(port_S) : -1);
                    if (QLC_MD.KetNoiToi_Server(ip_S, port_I)) {
                        QLC_V.AnHien_TrangThai(true);
                        QLC_V.CapNhat_ThongBao_TrangThai(true);
                        ThuMucModel ThuMuc = QLC_MD.CayThuMuc_Client();
                        CayThuMucView CayThuMuc = new CayThuMucView(ThuMuc, QLC_V);
                        QLC_V.CapNhapLog(QLC_MD.LayDanhSach_TapTinLog());
                        QLC_V.HienThiCayThuMuc(CayThuMuc);

                        ThreadLangNgheThayDoiTapTin LangNghe_ThayDoi_TapTin = new ThreadLangNgheThayDoiTapTin(QLC_MD, QLC_V, QLC_MD.LayServer(), QLC_MD.LayTenThuMuc_TheoDoi());
                        LangNghe_ThayDoi_TapTin.start();
                    } else {
                        QLC_V.Dialog_ThongBao("Kết nối tới Server không thành công!!!");
                    }
                }
                return;
            case "Stop":
                QLC_MD.NgatKetNoi_Server();
                QLC_V.AnHien_TrangThai(false);
                QLC_V.CapNhat_ThongBao_TrangThai(false);
                QLC_V.CapNhapLog(QLC_MD.LayDanhSach_TapTinLog());
                return;
            default:
                return;
        }
    }

    private boolean KiemTraSo(String so) {
        try {
            Integer.parseInt(so);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        QLC_MD.NgatKetNoi_Server();
        QLC_V.AnHien_TrangThai(false);
        QLC_V.CapNhat_ThongBao_TrangThai(false);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
