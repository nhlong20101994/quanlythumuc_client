
package model;

import java.io.Serializable;

public class DuLieuTruyenQuaMangModel implements Serializable {
    private String TieuDe;
    private ThuMucModel CayThuMuc;
    private String NoiDung;
    private TapTinLogModel TapTinLog;
    
    public DuLieuTruyenQuaMangModel(String tieude, ThuMucModel caythumuc, String noidung, TapTinLogModel taptinlog) {
        this.TieuDe = tieude;
        this.CayThuMuc = caythumuc;
        this.NoiDung = noidung;
        this.TapTinLog = taptinlog;
    }
    
    public void SetTieuDe(String tieude) {
        this.TieuDe = tieude;
    }
    
    public void SetCayThuMuc(ThuMucModel caythumuc) {
        this.CayThuMuc = caythumuc;
    }
    
    public void SetNoiDung(String noidung) {
        this.NoiDung = noidung;
    }
    
    public void SetTapTinLog(TapTinLogModel taptinlog) {
        this.TapTinLog = taptinlog;
    }
    
    public String GetTieuDe() {
        return this.TieuDe;
    }
    
    public ThuMucModel GetCayThuMuc() {
        return this.CayThuMuc;
    }
    
    public String GetNoiDung() {
        return this.NoiDung;
    }
    
    public TapTinLogModel GetTapTinLog() {
        return this.TapTinLog;
    }
}
