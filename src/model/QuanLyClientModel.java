package model;

import java.io.*;
import java.net.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QuanLyClientModel {

    private Socket Server;
    private String TenThuMuc_TheoDoi_MacDinh = "C:/ClientMonitoringSystem/Data";
    private String TenThuMuc_GhiLog_MacDinh = "C:/ClientMonitoringSystem/Log";
    private String TenTapTin_GhiLog_MacDinh = "C:/ClientMonitoringSystem/Log/TapTinLog.txt";
    public static List<TapTinLogModel> DanhSachTapTinLog = new ArrayList<>();
    private CayThuMucTheoDoiModel CTMTD_MD;

    public QuanLyClientModel() {
        TaoThuMuc(TenThuMuc_GhiLog_MacDinh);
        TaoTapTin(TenTapTin_GhiLog_MacDinh);
        DanhSachTapTinLog = DocTapTinLog();
    }

    public String[][] LayDanhSach_TapTinLog() {
        String[][] DSTL = null;
        if (DanhSachTapTinLog.size() <= 0) {
            return new String[][]{};
        }

        DSTL = new String[DanhSachTapTinLog.size()][4];
        int i = 0;
        for (TapTinLogModel log : DanhSachTapTinLog) {
            DSTL[i] = new String[]{"<html><center>" + log.GetSTT(),
                "<html><center>" + DinhDang_ThoiGian(log.GetThoiDiem()),
                "<html><center>" + log.GetHanhDong(),
                "<html><center>" + log.GetDienGiaiHanhDong()};
            i++;
        }

        return DSTL;
    }

    public boolean KetNoiToi_Server(String ip, int port) {
        try {
            Server = new Socket(ip, port);

            TaoThuMuc(TenThuMuc_TheoDoi_MacDinh);
            CapNhat_ThuMuc_TheoDoi();

            ThuMucModel CayThuMuc = CTMTD_MD.LayCayThuMuc_TheoDoi();
            TapTinLogModel Log = new TapTinLogModel(DanhSachTapTinLog.size() + "", LocalDateTime.now(), "Kết nối", "", 0, "Xin kết nối đến Server");
            DanhSachTapTinLog.add(Log);
            LuuTapTinLog(DanhSachTapTinLog);
            DuLieuTruyenQuaMangModel DuLieu_Truyen = new DuLieuTruyenQuaMangModel("Kết nối", CayThuMuc, "Xin kết nối.", Log);

            ThreadGhi XinKetNoi = new ThreadGhi(Server, DuLieu_Truyen);
            XinKetNoi.start();
            XinKetNoi.interrupt();

            ThreadDoc LangNghe_Server = new ThreadDoc(Server);
            LangNghe_Server.start();

            System.out.println("Client kết nối tới Server...");
        } catch (IOException ex) {
            System.out.println("error1: " + ex.getMessage());
            return false;
        } catch (IllegalArgumentException ex) {
            System.out.println("error2: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public void NgatKetNoi_Server() {
        try {
            TapTinLogModel Log = new TapTinLogModel(DanhSachTapTinLog.size() + "", LocalDateTime.now(), "Ngắt kết nối", "", 0, "Xin ngắt kết nối.");
            DanhSachTapTinLog.add(Log);
            LuuTapTinLog(DanhSachTapTinLog);
            
            DuLieuTruyenQuaMangModel DuLieu_Truyen = new DuLieuTruyenQuaMangModel("Ngắt kết nối", null, "Xin ngắt kết nối.", Log);
            
            ThreadGhi XinKetNoi = new ThreadGhi(Server, DuLieu_Truyen);
            XinKetNoi.start();
            XinKetNoi.interrupt();
            Server.close();
        } catch (IOException ex) {
            System.out.println("error: " + ex.getMessage());
        }
    }

    public Socket LayServer() {
        return Server;
    }

    public String LayTenThuMuc_TheoDoi() {
        return TenThuMuc_TheoDoi_MacDinh;
    }

    public CayThuMucTheoDoiModel LayCayThuMuc_TheoDoi_ThayDoi() {
        return new CayThuMucTheoDoiModel(Paths.get(TenThuMuc_TheoDoi_MacDinh));
    }

    public void CapNhat_ThuMuc_TheoDoi() {
        CTMTD_MD = new CayThuMucTheoDoiModel(Paths.get(TenThuMuc_TheoDoi_MacDinh));
    }

    public void CapNhat_ThongBao_Server(String noidung, TapTinLogModel log) {
        ThuMucModel CayThuMuc_Moi = CTMTD_MD.LayCayThuMuc_TheoDoi();
        DuLieuTruyenQuaMangModel DuLieu_Truyen = new DuLieuTruyenQuaMangModel(noidung, CayThuMuc_Moi, "", log);

        ThreadGhi CapNhat_ThuMuc = new ThreadGhi(Server, DuLieu_Truyen);
        CapNhat_ThuMuc.start();
        CapNhat_ThuMuc.interrupt();
    }

    public ThuMucModel CayThuMuc_Client() {
        return CTMTD_MD.LayCayThuMuc_TheoDoi();
    }

    private void TaoThuMuc(String thumuc) {
        if (new File(thumuc).isDirectory()) {
            return;
        }
        String ThuMuc_XuLy = "";
        String[] DanhSachTen_ThuMuc = thumuc.split("/");
        for (String s : DanhSachTen_ThuMuc) {
            ThuMuc_XuLy = ThuMuc_XuLy + s + "/";
            File file = new File(ThuMuc_XuLy);
            if (!file.isDirectory()) {
                file.mkdir();
            }
        }
    }

    private void TaoTapTin(String file_name) {
        File file = new File(file_name);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Tap tin loi" + e);
            }
        }
    }

    public void LuuTapTinLog(List<TapTinLogModel> taptinlog) {
        GhiTapTinNhiPhan(taptinlog, TenTapTin_GhiLog_MacDinh);
    }

    public List<TapTinLogModel> DocTapTinLog() {
        return DocTapTinNhiPhan(TenTapTin_GhiLog_MacDinh);
    }

    private void GhiTapTinNhiPhan(List<TapTinLogModel> taptinlog, String diachi_taptin) {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(diachi_taptin);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(taptinlog);
            System.out.println("Ghi tap tin thanh cong...");
        } catch (FileNotFoundException e) {
            System.out.println("Khong tìm thay tap tin: " + e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeOS(oos);
            closeOS(fos);
        }
    }

    private List<TapTinLogModel> DocTapTinNhiPhan(String diachi_taptin) {
        List<TapTinLogModel> TapTinLog = new ArrayList<>();
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        if (KiemTraTapTin(diachi_taptin) == false) {
            return TapTinLog;
        }
        try {
            fis = new FileInputStream(diachi_taptin);
            ois = new ObjectInputStream(fis);
            TapTinLog = (List<TapTinLogModel>) ois.readObject();
            System.out.println("Doc danh sach hoc sinh thanh cong...");
        } catch (Exception e) {
            System.out.println("Doc danh sach hoc sinh loi...");
            return TapTinLog;
        } finally {
            closeIS(ois);
            closeIS(fis);
        }
        return TapTinLog;
    }

    public String DinhDang_ThoiGian(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    }

    private boolean KiemTraTapTin(String file_name) {
        File file = new File(file_name);
        if (!file.exists()) {
            return false;
        }
        return true;
    }

    private void closeOS(OutputStream os) {
        try {
            if (os != null) {
                os.close();
            }
        } catch (Exception e) {
            System.out.println("Error Close file output: " + e);
        }
    }

    private void closeIS(InputStream is) {
        try {
            if (is != null) {
                is.close();
            }
        } catch (Exception e) {
            System.out.println("Error Close file input: " + e);
        }
    }
}
