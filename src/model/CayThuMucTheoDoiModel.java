package model;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class CayThuMucTheoDoiModel implements Serializable {

    private Path TenDuongDan;
    private String TenThuMuc_TheoDoi;
    private ThuMucModel CayThuMuc_TheoDoi;

    public CayThuMucTheoDoiModel(Path tenduongdan) {
        TenDuongDan = tenduongdan;
        CayThuMuc_TheoDoi = XuLy_CayThuMuc(TenDuongDan);
    }

    public ThuMucModel LayCayThuMuc_TheoDoi() {
        return CayThuMuc_TheoDoi;
    }

    private ThuMucModel XuLy_CayThuMuc(Path tenduongdan) {
        List<ThuMucModel> DanhSach_ThuMuc = new ArrayList<>();
        List<TapTinModel> DanhSach_TapTin = new ArrayList<>();
        ThuMucModel ThuMuc_TheoDoi = Them_ThuMuc_Model(tenduongdan);
        File file = new File(tenduongdan.toString());
        File[] files = file.listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                DanhSach_ThuMuc.add(XuLy_CayThuMuc(Paths.get(f.getPath())));
            }
            if (f.isFile()) {
                DanhSach_TapTin.add(Them_TapTin_Model(Paths.get(f.getPath())));
            }
        }
        ThuMuc_TheoDoi.DanhSach_ThuMuc_TapTin(DanhSach_TapTin, DanhSach_ThuMuc);
        return ThuMuc_TheoDoi;
    }

    private ThuMucModel Them_ThuMuc_Model(Path tenduongdan) {
        ThuMucModel ThuMuc = null;
        BasicFileAttributes attr;
        try {
            attr = Files.readAttributes(tenduongdan, BasicFileAttributes.class);
            LocalDateTime ThoiGian_KhoiTao = formatDateTime(attr.creationTime());
            LocalDateTime LanCuoi_TruyCap = formatDateTime(attr.lastAccessTime());
            LocalDateTime LanCuoi_ChinhSua = formatDateTime(attr.lastModifiedTime());
            String TenThuMuc = tenduongdan.getFileName().toString();
            long KichThuoc = attr.size();
            ThuMuc = new ThuMucModel(tenduongdan.toString(), ThoiGian_KhoiTao, LanCuoi_TruyCap, LanCuoi_ChinhSua, TenThuMuc, KichThuoc);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return ThuMuc;
    }
    
    private TapTinModel Them_TapTin_Model(Path tenduongdan) {
        TapTinModel TapTin = null;
        BasicFileAttributes attr;
        try {
            attr = Files.readAttributes(tenduongdan, BasicFileAttributes.class);
            LocalDateTime ThoiGian_KhoiTao = formatDateTime(attr.creationTime());
            LocalDateTime LanCuoi_TruyCap = formatDateTime(attr.lastAccessTime());
            LocalDateTime LanCuoi_ChinhSua = formatDateTime(attr.lastModifiedTime());
            String GetName = tenduongdan.getFileName().toString();
            String DinhDang = GetName.split("\\.")[1];
            String TenTapTin = GetName.split("\\.")[0];
            long KichThuoc = attr.size();
            TapTin = new TapTinModel(tenduongdan.toString(), ThoiGian_KhoiTao, LanCuoi_TruyCap, LanCuoi_ChinhSua, DinhDang, TenTapTin, KichThuoc);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return TapTin;
    }
    
    public LocalDateTime formatDateTime(FileTime fileTime) {

        LocalDateTime localDateTime = fileTime
                .toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        return localDateTime;
        //System.out.println("localDateTime: " + localDateTime);
        //return localDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    }
}
