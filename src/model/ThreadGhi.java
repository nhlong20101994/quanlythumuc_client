
package model;

import java.io.*;
import java.net.*;

public class ThreadGhi extends Thread {
    private Socket Server;
    private DuLieuTruyenQuaMangModel DuLieuGui;

    public ThreadGhi(Socket socket, DuLieuTruyenQuaMangModel dulieugui) {
        this.Server = socket;
        this.DuLieuGui = dulieugui;
    }

    @Override
    public void run() {
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(Server.getOutputStream());
            oos.writeObject(DuLieuGui);
            System.out.println("Đã gửi đến Server thành công");
        } catch (Exception e) {
            try {
                oos.close();
            } catch (Exception ex) {
                System.out.println("Ngắt kết nối Server.");
            }
        }
    }
}
