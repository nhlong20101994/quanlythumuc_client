package model;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import view.CayThuMucView;
import view.QuanLyClientView;

public class ThreadLangNgheThayDoiTapTin extends Thread {

    private String ThuMuc_TheoDoi;
    private LocalDateTime ThoiGian_KhoiTao;
    private LocalDateTime LanCuoi_TruyCap;
    private LocalDateTime LanCuoi_ChinhSua;
    private Socket Server;
    private QuanLyClientModel QLC_MD;
    private QuanLyClientView QLC_V;

    public ThreadLangNgheThayDoiTapTin(QuanLyClientModel qlc_md, QuanLyClientView qlc_v, Socket server, String thumuc_theodoi) {
        Server = server;
        QLC_MD = qlc_md;
        QLC_V = qlc_v;
        ThuMuc_TheoDoi = thumuc_theodoi;
        LayThoiGian_ThuMuc();
    }

    private void LayThoiGian_ThuMuc() {
        try {
            BasicFileAttributes attr = Files.readAttributes(Paths.get(ThuMuc_TheoDoi), BasicFileAttributes.class);
            ThoiGian_KhoiTao = formatDateTime(attr.creationTime());
            LanCuoi_TruyCap = formatDateTime(attr.lastAccessTime());
            LanCuoi_ChinhSua = formatDateTime(attr.lastModifiedTime());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                BasicFileAttributes attr = Files.readAttributes(Paths.get(ThuMuc_TheoDoi), BasicFileAttributes.class);
                LocalDateTime LanCuoi_ChinhSua_Moi = formatDateTime(attr.lastModifiedTime());
                if (!LanCuoi_ChinhSua.toString().equals(LanCuoi_ChinhSua_Moi.toString())) {
                    System.out.println("- Thư mục có sự thay đổi: Cần gửi thông báo đến Server.");
                    System.out.println("  + LanCuoi_ChinhSua: " + LanCuoi_ChinhSua);
                    System.out.println("  + LanCuoi_ChinhSua_Moi: " + LanCuoi_ChinhSua_Moi);
                    CayThuMucTheoDoiModel CayThuMuc_TheoDoi_Moi = QLC_MD.LayCayThuMuc_TheoDoi_ThayDoi();
                    ThuMucModel CayThuMuc_KhoiTao = QLC_MD.CayThuMuc_Client();
                    ThuMucModel CayThuMuc_ThayDoi = CayThuMuc_TheoDoi_Moi.LayCayThuMuc_TheoDoi();
                    KiemTra_ThayDoi_ThuMuc(CayThuMuc_ThayDoi, CayThuMuc_KhoiTao);
                    QLC_MD.CapNhat_ThuMuc_TheoDoi();
                    ThuMucModel ThuMuc = QLC_MD.CayThuMuc_Client();
                    CayThuMucView CayThuMuc = new CayThuMucView(ThuMuc, QLC_V);
                    QLC_V.HienThiCayThuMuc(CayThuMuc);
                    LanCuoi_ChinhSua = LanCuoi_ChinhSua_Moi;
                }
                sleep(2000);
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void KiemTra_ThayDoi_ThuMuc(ThuMucModel caythumuc_thaydoi, ThuMucModel caythumuc_khoitao) {
        if (caythumuc_thaydoi.LayDanhSach_ThuMuc().size() > caythumuc_khoitao.LayDanhSach_ThuMuc().size()) {
            int m;
            int n;
            for (m = 0; m < caythumuc_thaydoi.LayDanhSach_ThuMuc().size(); m++) {
                boolean XoaI = false;
                for (n = 0; n < caythumuc_khoitao.LayDanhSach_ThuMuc().size(); n++) {
                    if (caythumuc_thaydoi.LayDanhSach_ThuMuc().get(m).LayTenThuMuc()
                            .equals(caythumuc_khoitao.LayDanhSach_ThuMuc().get(n).LayTenThuMuc())) {
                        XoaI = true;
                        if (!caythumuc_thaydoi.LayDanhSach_ThuMuc().get(m).LayThoiGian_KhoiTao()
                                .equals(caythumuc_khoitao.LayDanhSach_ThuMuc().get(n).LayThoiGian_KhoiTao())) {
                            TapTinLogModel Log = new TapTinLogModel(
                                    QuanLyClientModel.DanhSachTapTinLog.size() + "",
                                    LocalDateTime.now(), "Thêm thư mục", null, 0, "Thêm thư mục mới vào thư mục quản lý");
                            QuanLyClientModel.DanhSachTapTinLog.add(Log);
                            QLC_MD.LuuTapTinLog(QuanLyClientModel.DanhSachTapTinLog);
                            QLC_V.CapNhapLog(QLC_MD.LayDanhSach_TapTinLog());
                            QLC_MD.CapNhat_ThuMuc_TheoDoi();
                            QLC_MD.CapNhat_ThongBao_Server("Thêm", Log);
                        } else {
                            if (!caythumuc_thaydoi.LayDanhSach_ThuMuc().get(m).LayLanCuoi_ChinhSua()
                                    .equals(caythumuc_khoitao.LayDanhSach_ThuMuc().get(n).LayLanCuoi_ChinhSua())) {
                                KiemTra_ThayDoi_ThuMuc(caythumuc_thaydoi.LayDanhSach_ThuMuc().get(m), caythumuc_khoitao.LayDanhSach_ThuMuc().get(n));
                            }
                        }
                    } else {
                        if (caythumuc_thaydoi.LayDanhSach_ThuMuc().get(m).LayThoiGian_KhoiTao()
                                .equals(caythumuc_khoitao.LayDanhSach_ThuMuc().get(n).LayThoiGian_KhoiTao())) {
                            TapTinLogModel Log = new TapTinLogModel(
                                    QuanLyClientModel.DanhSachTapTinLog.size() + "",
                                    LocalDateTime.now(), "Đổi tên thư mục", null, 0, "Thư mục : " + caythumuc_khoitao.LayDanhSach_ThuMuc().get(n).LayTenThuMuc() + " đổi tên thành " + caythumuc_thaydoi.LayDanhSach_ThuMuc().get(n).LayTenThuMuc());
                            QuanLyClientModel.DanhSachTapTinLog.add(Log);
                            QLC_MD.LuuTapTinLog(QuanLyClientModel.DanhSachTapTinLog);
                            QLC_V.CapNhapLog(QLC_MD.LayDanhSach_TapTinLog());
                            QLC_MD.CapNhat_ThuMuc_TheoDoi();
                            QLC_MD.CapNhat_ThongBao_Server("Đổi tên", Log);
                        }
                    }
                }
                if (!XoaI) {
                    TapTinLogModel Log = new TapTinLogModel(
                            QuanLyClientModel.DanhSachTapTinLog.size() + "",
                            LocalDateTime.now(), "Thêm thư mục", null, 0, "Thêm thư mục mới vào thư mục quản lý");
                    QuanLyClientModel.DanhSachTapTinLog.add(Log);
                    QLC_MD.LuuTapTinLog(QuanLyClientModel.DanhSachTapTinLog);
                    QLC_V.CapNhapLog(QLC_MD.LayDanhSach_TapTinLog());
                    QLC_MD.CapNhat_ThuMuc_TheoDoi();
                    QLC_MD.CapNhat_ThongBao_Server("Thêm", Log);
                }
            }
        } else {
            if (caythumuc_thaydoi.LayDanhSach_ThuMuc().size() < caythumuc_khoitao.LayDanhSach_ThuMuc().size()) {
                int i;
                int j;

                for (i = 0; i < caythumuc_khoitao.LayDanhSach_ThuMuc().size(); i++) {
                    boolean XoaI = false;
                    for (j = 0; j < caythumuc_thaydoi.LayDanhSach_ThuMuc().size(); j++) {
                        if (caythumuc_khoitao.LayDanhSach_ThuMuc().get(i).LayTenThuMuc()
                                .equals(caythumuc_thaydoi.LayDanhSach_ThuMuc().get(j).LayTenThuMuc())) {
                            XoaI = true;
                            if (!caythumuc_khoitao.LayDanhSach_ThuMuc().get(i).LayThoiGian_KhoiTao()
                                    .equals(caythumuc_thaydoi.LayDanhSach_ThuMuc().get(j).LayThoiGian_KhoiTao())) {
                                TapTinLogModel Log = new TapTinLogModel(
                                        QuanLyClientModel.DanhSachTapTinLog.size() + "",
                                        LocalDateTime.now(), "Thêm thư mục", null, 0, "Thêm thư mục mới vào thư mục quản lý");
                                QuanLyClientModel.DanhSachTapTinLog.add(Log);
                                QLC_MD.LuuTapTinLog(QuanLyClientModel.DanhSachTapTinLog);
                                QLC_V.CapNhapLog(QLC_MD.LayDanhSach_TapTinLog());
                                QLC_MD.CapNhat_ThuMuc_TheoDoi();
                                QLC_MD.CapNhat_ThongBao_Server("Thêm", Log);
                            } else {
                                if (!caythumuc_khoitao.LayDanhSach_ThuMuc().get(i).LayLanCuoi_ChinhSua()
                                        .equals(caythumuc_thaydoi.LayDanhSach_ThuMuc().get(j).LayLanCuoi_ChinhSua())) {
                                    KiemTra_ThayDoi_ThuMuc(caythumuc_thaydoi.LayDanhSach_ThuMuc().get(j), caythumuc_khoitao.LayDanhSach_ThuMuc().get(i));
                                }
                            }
                        }
                    }
                    if (!XoaI) {
                        TapTinLogModel Log = new TapTinLogModel(
                                QuanLyClientModel.DanhSachTapTinLog.size() + "",
                                LocalDateTime.now(), "Xóa thư mục", null, 0, "Thư mục bị xóa: " + caythumuc_khoitao.LayDanhSach_ThuMuc().get(i).LayTenThuMuc());
                        QuanLyClientModel.DanhSachTapTinLog.add(Log);
                        QLC_MD.LuuTapTinLog(QuanLyClientModel.DanhSachTapTinLog);
                        QLC_V.CapNhapLog(QLC_MD.LayDanhSach_TapTinLog());
                        QLC_MD.CapNhat_ThuMuc_TheoDoi();
                        QLC_MD.CapNhat_ThongBao_Server("Xóa", Log);
                    }
                }
            } else {
                int k;
                int l;
                for (k = 0; k < caythumuc_thaydoi.LayDanhSach_ThuMuc().size(); k++) {
                    for (l = 0; l < caythumuc_khoitao.LayDanhSach_ThuMuc().size(); l++) {
                        if (caythumuc_thaydoi.LayDanhSach_ThuMuc().get(k).LayTenThuMuc()
                                .equals(caythumuc_khoitao.LayDanhSach_ThuMuc().get(l).LayTenThuMuc())) {
                            if (!caythumuc_thaydoi.LayDanhSach_ThuMuc().get(k).LayThoiGian_KhoiTao()
                                    .equals(caythumuc_khoitao.LayDanhSach_ThuMuc().get(l).LayThoiGian_KhoiTao())) {
                                TapTinLogModel Log = new TapTinLogModel(
                                        QuanLyClientModel.DanhSachTapTinLog.size() + "",
                                        LocalDateTime.now(), "Thêm thư mục", null, 0, "Thêm thư mục mới vào thư mục quản lý");
                                QuanLyClientModel.DanhSachTapTinLog.add(Log);
                                QLC_MD.LuuTapTinLog(QuanLyClientModel.DanhSachTapTinLog);
                                QLC_V.CapNhapLog(QLC_MD.LayDanhSach_TapTinLog());
                                QLC_MD.CapNhat_ThuMuc_TheoDoi();
                                QLC_MD.CapNhat_ThongBao_Server("Thêm", Log);
                            } else {
                                if (!caythumuc_thaydoi.LayDanhSach_ThuMuc().get(k).LayLanCuoi_ChinhSua()
                                        .equals(caythumuc_khoitao.LayDanhSach_ThuMuc().get(l).LayLanCuoi_ChinhSua())) {
                                    KiemTra_ThayDoi_ThuMuc(caythumuc_thaydoi.LayDanhSach_ThuMuc().get(k), caythumuc_khoitao.LayDanhSach_ThuMuc().get(l));
                                }
                            }
                        } else {
                            if (caythumuc_thaydoi.LayDanhSach_ThuMuc().get(k).LayThoiGian_KhoiTao()
                                    .equals(caythumuc_khoitao.LayDanhSach_ThuMuc().get(l).LayThoiGian_KhoiTao())) {
                                TapTinLogModel Log = new TapTinLogModel(
                                        QuanLyClientModel.DanhSachTapTinLog.size() + "",
                                        LocalDateTime.now(), "Đổi tên thư mục", null, 0, "Thư mục : " + caythumuc_khoitao.LayDanhSach_ThuMuc().get(l).LayTenThuMuc() + " đổi tên thành " + caythumuc_thaydoi.LayDanhSach_ThuMuc().get(l).LayTenThuMuc());
                                QuanLyClientModel.DanhSachTapTinLog.add(Log);
                                QLC_MD.LuuTapTinLog(QuanLyClientModel.DanhSachTapTinLog);
                                QLC_V.CapNhapLog(QLC_MD.LayDanhSach_TapTinLog());
                                QLC_MD.CapNhat_ThuMuc_TheoDoi();
                                QLC_MD.CapNhat_ThongBao_Server("Đổi tên", Log);
                            }
                        }
                    }
                }
            }
        }
        KiemTra_ThayDoi_TapTin(caythumuc_thaydoi.LayDanhSach_TapTin(), caythumuc_khoitao.LayDanhSach_TapTin());
    }

    public void KiemTra_ThayDoi_TapTin(List<TapTinModel> taptin_thaydoi, List<TapTinModel> taptin_khoitao) {
        if (taptin_thaydoi.size() > taptin_khoitao.size()) {
            int m;
            int n;
            for (m = 0; m < taptin_thaydoi.size(); m++) {
                for (n = 0; n < taptin_khoitao.size(); n++) {
                    if (taptin_thaydoi.get(m).LayTenTapTin()
                            .equals(taptin_khoitao.get(n).LayTenTapTin())) {
                        if (!taptin_thaydoi.get(m).LayThoiGian_KhoiTao()
                                .equals(taptin_khoitao.get(n).LayThoiGian_KhoiTao())) {
                            System.out.println("Tập tin thêm mới: " + taptin_thaydoi.get(n).LayTenTapTin());
                        } else {
                            if (!taptin_thaydoi.get(m).LayLanCuoi_ChinhSua()
                                    .equals(taptin_khoitao.get(n).LayLanCuoi_ChinhSua())) {
                                System.out.println("Tập tin được chỉnh sửa: " + taptin_thaydoi.get(n).LayTenTapTin());
                            }
                        }
                    } else {
                        if (taptin_thaydoi.get(m).LayThoiGian_KhoiTao()
                                .equals(taptin_khoitao.get(n).LayThoiGian_KhoiTao())) {
                            System.out.println("Thư mục : " + taptin_khoitao.get(n).LayTenTapTin() + " đổi tên thành " + taptin_thaydoi.get(n).LayTenTapTin());
                        }
                    }
                }
                System.out.println("Thư mục được thêm mới: " + taptin_thaydoi.get(m).LayTenTapTin());
            }
        } else {
            if (taptin_thaydoi.size() < taptin_khoitao.size()) {
                int i;
                int j;
                for (i = 0; i < taptin_khoitao.size(); i++) {
                    for (j = 0; j < taptin_thaydoi.size(); j++) {
                        if (taptin_khoitao.get(i).LayTenTapTin()
                                .equals(taptin_thaydoi.get(j).LayTenTapTin())) {
                            if (!taptin_khoitao.get(i).LayThoiGian_KhoiTao()
                                    .equals(taptin_thaydoi.get(j).LayThoiGian_KhoiTao())) {
                                System.out.println("Thư mục thêm mới: " + taptin_thaydoi.get(j).LayTenTapTin());
                            } else {
                                if (!taptin_khoitao.get(i).LayLanCuoi_ChinhSua()
                                        .equals(taptin_thaydoi.get(j).LayLanCuoi_ChinhSua())) {
                                    System.out.println("Tập tin được chỉnh sửa: " + taptin_thaydoi.get(j).LayTenTapTin());
                                }
                            }
                        }
                    }
                    System.out.println("Thư mục bị xóa: " + taptin_khoitao.get(i).LayTenTapTin());
                }
            } else {
                int k;
                int l;
                for (k = 0; k < taptin_thaydoi.size(); k++) {
                    for (l = 0; l < taptin_khoitao.size(); l++) {
                        if (taptin_thaydoi.get(k).LayTenTapTin()
                                .equals(taptin_khoitao.get(l).LayTenTapTin())) {
                            if (!taptin_thaydoi.get(k).LayThoiGian_KhoiTao()
                                    .equals(taptin_khoitao.get(l).LayThoiGian_KhoiTao())) {
                                System.out.println("Thư mục thêm mới: " + taptin_thaydoi.get(l).LayTenTapTin());
                            } else {
                                if (!taptin_thaydoi.get(k).LayLanCuoi_ChinhSua()
                                        .equals(taptin_khoitao.get(l).LayLanCuoi_ChinhSua())) {
                                    System.out.println("Tập tin được chỉnh sửa: " + taptin_thaydoi.get(k).LayTenTapTin());
                                }
                            }
                        } else {
                            if (taptin_thaydoi.get(k).LayThoiGian_KhoiTao()
                                    .equals(taptin_khoitao.get(l).LayThoiGian_KhoiTao())) {
                                System.out.println("Thư mục : " + taptin_khoitao.get(l).LayTenTapTin() + " đổi tên thành " + taptin_thaydoi.get(l).LayTenTapTin());
                            }
                        }
                    }
                }
            }
        }
    }

    public LocalDateTime formatDateTime(FileTime fileTime) {
        LocalDateTime localDateTime = fileTime
                .toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        // localDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        return localDateTime;
    }
}
