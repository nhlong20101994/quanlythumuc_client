
package model;

import java.io.*;
import java.net.*;

public class ThreadDoc extends Thread {
    private Socket Client;

    public ThreadDoc(Socket client) {
        this.Client = client;
    }
    
    @Override
    public void run() {
        ObjectInputStream ois = null;
        DuLieuTruyenQuaMangModel DuLieu_Nhan = null;
        try {
            while (true) {
                ois = new ObjectInputStream(Client.getInputStream());
                DuLieu_Nhan = (DuLieuTruyenQuaMangModel)ois.readObject();
                String LuaChon = DuLieu_Nhan.GetTieuDe();
                switch (LuaChon) {
                    case "Kết nối":
                        System.out.println("Nội dung phản hồi từ server: " + DuLieu_Nhan.GetNoiDung());
                        break;
                    case "Ngắt kết nối":
                        
                        break;
                    case "Gửi lại thư mục":
                        
                        break;
                    case "Thêm":
                        
                        break;
                    case "Sửa":
                        
                        break;
                    case "Xóa":
                        
                        break;
                    case "Đổi tên":
                        
                        break;
                    default:
                        break;
                }

            }
        } catch (IOException ex) {
            try {
                if(ois != null) {
                    ois.close();
                }
            } catch (IOException e) {
                System.out.println("error: " + e.getMessage());
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("error: " + ex.getMessage());
        }
    }
}
