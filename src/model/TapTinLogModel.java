
package model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class TapTinLogModel implements Serializable {
    private String STT;
    private LocalDateTime ThoiDiem;
    private String HanhDong;
    private String Ip;
    private int Port;
    private String DienGiaiHanhDong;
    
    public TapTinLogModel() {
        
    }
    
    public TapTinLogModel(String stt, LocalDateTime thoidiem, String hanhdong, String ip, int port, String diengiaihanhdong) {
        this.STT = stt;
        this.ThoiDiem = thoidiem;
        this.HanhDong = hanhdong;
        this.Ip = ip;
        this.Port = port;
        this.DienGiaiHanhDong = diengiaihanhdong;
    }
    
    public String GetSTT() {
        return this.STT;
    }
    
    public LocalDateTime GetThoiDiem() {
        return this.ThoiDiem;
    }
    
    public String GetHanhDong() {
        return this.HanhDong;
    }
    
    public String GetIp() {
        return this.Ip;
    }
    
    public int GetPort() {
        return this.Port;
    }
    
    public String GetDienGiaiHanhDong() {
        return this.DienGiaiHanhDong;
    }
    
    public void SetSTT(String stt) {
        this.STT = stt;
    }
    
    public void SetThoiDiem(LocalDateTime thoidiem) {
        this.ThoiDiem = thoidiem;
    }
    
    public void SetHanhDong(String hanhdong) {
        this.HanhDong = hanhdong;
    }
    
    public void SetIp(String ip) {
        this.Ip = ip;
    }
    
    public void SetPort(int port) {
        this.Port = port;
    }
    
    public void SetDienGiaiHanhDong(String diengiaihanhdong) {
        this.DienGiaiHanhDong = diengiaihanhdong;
    }
}
